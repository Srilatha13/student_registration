import React from 'react'
import { Route, BrowserRouter as Router, Routes } from "react-router-dom"
import Form from './pages/form/Form.jsx'
import Display from './pages/display/Display.jsx'
const App = () => {

  return (
    <div>
     <Router>
      <Routes>

      
      <Route path="/" element={<Form/>}/>
      <Route path="/display" element={<Display/>}/>
      </Routes>
     </Router>
    </div>
  )
}

export default App
