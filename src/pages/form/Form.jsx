import React, { useState } from 'react'
import "./form.css"
import { useNavigate } from 'react-router-dom';

const Form = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        name: '',
        age: '',
        college: '',
        registrationNumber: '',
        yearOfJoining: '',
      });
    
      
      const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
          ...prevData,
          [name]: value,
        }));
      };
    
     
      const handleFormSubmit = (e) => {
        e.preventDefault();
        
        
        console.log("Name: " + formData.name);
        console.log("Age: " + formData.age);
        console.log("College: " + formData.college);
        console.log("Registration Number: " + formData.registrationNumber);
        console.log("Year of Joining: " + formData.yearOfJoining);
        navigate("/display", { state: formData });
      };
  return (
      

    <div>
      <h2>Student Registration Form</h2>
      <form onSubmit={handleFormSubmit}>
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          name="name"
          value={formData.name}
          onChange={handleInputChange}
          required
        />

        <label htmlFor="age">Age:</label>
        <input
          type="number"
          id="age"
          name="age"
          value={formData.age}
          onChange={handleInputChange}
          required
        />

        <label htmlFor="college">College:</label>
        <input
          type="text"
          id="college"
          name="college"
          value={formData.college}
          onChange={handleInputChange}
          required
        />

        <label htmlFor="registrationNumber">Registration Number:</label>
        <input
          type="text"
          id="registrationNumber"
          name="registrationNumber"
          value={formData.registrationNumber}
          onChange={handleInputChange}
          required
        />

        <label htmlFor="yearOfJoining">Year of Joining:</label>
        <input
          type="number"
          id="yearOfJoining"
          name="yearOfJoining"
          value={formData.yearOfJoining}
          onChange={handleInputChange}
          required
        />

        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default Form
