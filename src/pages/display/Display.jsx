import React from 'react'
import { useLocation } from 'react-router-dom';
import "./display.css"
const Display = () => {
    const location = useLocation();
  const data = location.state;
  return (
    <div class="container">
    <h2>Submitted Data</h2>

    <div>
        <p><strong>Name:</strong> {data.name}</p>
        <p><strong>Age:</strong> {data.age}</p>
        <p><strong>College:</strong> {data.college}</p>
        <p><strong>Registration Number:</strong> {data.registrationNumber}</p>
        <p><strong>Year of Joining:</strong> {data.yearOfJoining}</p>
    </div>
</div>
  )
}

export default Display
